import React from 'react'

const myStyle = {
    color: 'yellow',
    backgroundColor: 'black'
}

export default function Header() {
    let isRed = false
    return (
        <div className="center">
            <h1 style={{ color: 'red', backgroundColor: 'blue' }}>Hello</h1>
             <h1 style={myStyle}>Hello {isRed ? 'Red' : 'Not Red'}</h1>
        </div>
    )
}
