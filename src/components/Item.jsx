import React from 'react'
import {Card,Button} from 'react-bootstrap'

function Item(props) {
    let {img,title,description,disable} = props.data
    return (
        <div className={disable ? 'disappear' : 'appear'}>
        <Card>
            <Card.Img variant="top" src={img} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                {description}
                </Card.Text>
            </Card.Body>
        </Card>
        </div>
    )
}

export default Item
