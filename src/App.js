import { Col, Container, Row } from 'react-bootstrap';
import './App.css';
import {Button} from 'react-bootstrap'
import Item from './components/Item';
import NavMenu from './components/NavMenu';

import React, { Component } from 'react'

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          img: 'img/angkor.jpeg',
          title: 'Angkor',
          description: 'In cambodia',
          disable:false
        },
        {
          img: 'https://www.visitsoutheastasia.travel/wp-content/uploads/2019/10/Cambodia-Taprom-Temple-Siem-Reap.jpg',
          title: 'Taprom',
          description: 'In cambodia',
          disable:true
        },
        {
          img: 'https://www.visitsoutheastasia.travel/wp-content/uploads/2019/10/Cambodia-Taprom-Temple-Siem-Reap.jpg',
          title: 'Taprom',
          description: 'In cambodia',
          disable:false
        },
        {
          img: 'https://juliasalbum.com/wp-content/uploads/2019/07/Banteay-Srei-Lady-Temple-Cambodia-16.jpg',
          title: 'Banteay Srey',
          description: 'In cambodia',
          disable:false
        }
      ]
    } 
  }
  deleteRecord(index){
      let newRecord = [...this.state.data]
      newRecord[index].disable=true;
      this.setState({
        data:newRecord
      }, () => {
        console.log(this.state.data);
      })
  }
  render() {
    return (
      <>
        <NavMenu />
        <Container>
          <Row>
            {
              this.state.data.map((obj, idx) =>
                <Col key={idx} md={3} sm={6} className={obj.disable ? 'disappear' : 'appear'}>
                  <Item data={obj}/>
                  <Button onClick={()=>this.deleteRecord(idx)} variant="danger">Delete</Button>
                </Col>
                )
            }
          </Row>
        </Container>
      </>
    )
  }
}
